#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

REPORT_DIR=build/reports
mkdir -p "${REPORT_DIR}"
ruff --output-format=gitlab . >|"${REPORT_DIR}/ruff-codeclimate.json"
