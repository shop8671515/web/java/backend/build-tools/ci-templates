#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

REPORT_DIR=build/reports
mkdir -p "${REPORT_DIR}"
if ! mypy --no-error-summary . >|"${REPORT_DIR}/mypy"; then
	cat "${REPORT_DIR}/mypy"
	SCRIPT_DIR=$(dirname "$0")
	PYTHONHASHSEED=0 "${SCRIPT_DIR}/../mypy2codeclimate.py" <"${REPORT_DIR}/mypy" >|"${REPORT_DIR}/mypy-codeclimate.json"
	exit 1
fi

