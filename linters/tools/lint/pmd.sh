#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
PMD="6.55.0"
PMD_DIR="${HOME}/.cache/pmd-bin-${PMD}"
REPORT_DIR=build/reports
mkdir -p "${REPORT_DIR}"
if [[ -z ${CI+x} ]]; then
	# shellcheck source=/dev/null
	source .ci_env

	if [[ ! -d "${PMD_DIR}" ]]; then
		mkdir -p "${HOME}/.cache"
		wget "https://github.com/pmd/pmd/releases/download/pmd_releases%2F${PMD}/pmd-bin-${PMD}.zip" --directory-prefix "${HOME}/.cache"
		unzip "${HOME}/.cache/pmd-bin-${PMD}" -d "${HOME}/.cache"
	fi
fi

mkdir -p build/cache
"${HOME}/.cache/pmd-bin-${PMD}/bin/run.sh" pmd --cache build/cache/pmd --fail-on-violation false --format xml --report-file "${REPORT_DIR}/pmd.xml" --rulesets "${CI_PMD_RULESET:-pmd-custom.xml}" --file-list <(printf "%s\n" "$@") 2>/dev/null
"$(dirname "$0")/../report2codeclimate.py" --pmd "${REPORT_DIR}/pmd.xml" --quickfix-file - -o "${REPORT_DIR}/pmd-codeclimate.json"
