#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

REPORT_DIR=build/reports
mkdir -p "${REPORT_DIR}"
if ! pylint --recursive yes --output-format json . >|"${REPORT_DIR}/pylint.json"; then
	cat "${REPORT_DIR}/pylint.json"
	SCRIPT_DIR=$(dirname "$0")
	"${SCRIPT_DIR}/../report2codeclimate.py" --pylint "${REPORT_DIR}/pylint.json" --output "${REPORT_DIR}/pylint-codeclimate.json"
	exit 1
fi
