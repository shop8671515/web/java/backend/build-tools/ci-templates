#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

CODENARC=3.2.0
JAR="${HOME}/.cache/jars/codenarc.jar"
REPORT_DIR=build/reports
mkdir -p "${REPORT_DIR}"
if [[ -z ${CI+x} ]] && [[ ! -f "${JAR}" ]]; then
  mkdir -p "${HOME}/.cache/jars"
  wget "https://repo1.maven.org/maven2/org/codenarc/CodeNarc/${CODENARC}/CodeNarc-${CODENARC}-all.jar" -O "${JAR}"
fi
java -jar "${JAR}" -excludes='./codenarc.groovy' -rulesetfiles=file:codenarc.groovy -report="json:${REPORT_DIR}/codenarc.json" -report="html:${REPORT_DIR}/codenarc.html" -report=console

"$(dirname "$0")/../report2codeclimate.py" --codenarc "${REPORT_DIR}/codenarc.json" --quickfix "${REPORT_DIR}/quickfix" -o "${REPORT_DIR}/codenarc-codeclimate.json"