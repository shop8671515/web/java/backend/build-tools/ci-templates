#!/usr/bin/env python3
# :License: GNU General Public License version 3 or later
import itertools
import json
import logging
import subprocess
import sys
from argparse import ArgumentParser, Namespace
from collections import defaultdict
from typing import Any, Iterable, Mapping

import pglast

def get_logger(level=logging.INFO):
    logger = logging.getLogger(__file__)
    logger.setLevel(level)
    logger.addHandler(logging.StreamHandler())
    return logger


def cli() -> Namespace:
    parser = ArgumentParser(description="Waliduje zmiany plików SQL w recenzji")
    parser.add_argument("--base-sha", help="Bazowa rewizja", required=True, type=str)
    return parser.parse_args()


def str_const(arg):
    return arg["A_Const"]["sval"]["sval"]


def get_changed_translations(
        sql: str
) -> tuple[set[str], set[str], dict[str, set[str]]]:
    # deleteTranslation rozczarowująco nie przyjmuje nazwy słownika, a jedynie
    # nazwę klucza, więc usuwa wszystkie klucze we wszystkich słownikach
    updated_dictionaries = defaultdict(set)
    deleted_keys = set()
    deleted_dictionaries = set()
    tree = json.loads(pglast.parser.parse_sql_join(sql))
    for stmt in tree["stmts"]:
        stmt = stmt["stmt"]
        select = stmt.get("SelectStmt")
        if not select:
            continue
        target = select["targetList"][0].get("ResTarget")
        if not target:
            continue
        func_call = target["val"].get("FuncCall")
        if not func_call:
            continue
        name = func_call["funcname"][0]["String"]["sval"]
        args = func_call["args"]
        if name == "updateTranslation":
            dict_name = str_const(args[0])
            key = str_const(args[1])
            # translation = str_const(args[2])
            # lang = str_const(args[3])
            updated_dictionaries[dict_name].add(key)
        elif name == "deleteTranslation":
            key = str_const(args[0])
            deleted_keys.add(key)
        elif name == "deletedDictionary":
            dict_name = str_const(args[0])
            deleted_dictionaries.add(dict_name)

    return (deleted_keys, deleted_dictionaries, updated_dictionaries)


def main():
    cmd = cli()
    logger = get_logger()
    base_sha: str = cmd.base_sha
    modified = get_modified_sql_files(base_sha, logger)
    error_detected = False
    for path in modified:
        error_detected |= lint_sql_files(base_sha, path, logger)

    sys.exit(int(error_detected))


def lint_sql_files(base_sha: str, path: str, logger: logging.Logger) -> bool:
    old_file = git_get_file(base_sha, path)
    _, _, old_keys = get_changed_translations(old_file)

    with open(path, "r", encoding="utf8") as fp:
        new_file = fp.read()

    removed_keys, removed_dictionaries, new_keys = get_changed_translations(new_file)
    for removed_dictionary in removed_dictionaries:
        safe_del(old_keys, removed_dictionary)
    missing_keys = flat_set(old_keys) - flat_set(new_keys)
    unremoved_keys = missing_keys - removed_keys
    if unremoved_keys:
        print("Nie usunięte słowniki w", path)
        print("\n".join(sorted(unremoved_keys)))
    return bool(unremoved_keys)


def safe_del(d: dict[str, Any], k: str):
    if k in d:
        del d[k]


def flat_set(d: Mapping[Any, Iterable[str]]) -> set[str]:
    return set(itertools.chain(*d.values()))


def git_get_file(rev: str, path: str) -> str:
    resp = subprocess.run(
        ["git", "show", f"{rev}:{path}"], check=True, stdout=subprocess.PIPE
    )
    return resp.stdout.decode("utf8")


def get_modified_sql_files(bash_sha: str, logger: logging.Logger) -> list[str]:
    resp = subprocess.run(
        [
            "git",
            "diff",
            "--diff-filter",
            "adr",
            "--name-only",
            bash_sha,
            "*.sql"
        ],
        check=True,
        stdout=subprocess.PIPE,
    )
    files = resp.stdout.decode("utf8").rstrip().split("\n")
    try:
        files.remove("")
    except ValueError as _:
        pass
    logger.debug("Zmodyfikowane pliki SQL: %s", files)
    return files


if __name__ == "__main__":
    main()