#!/usr/bin/env python3
from __future__ import annotations

import subprocess
from argparse import ArgumentParser, Namespace
from pathlib import Path
from typing import TextIO


def cli() -> Namespace:
    parser = ArgumentParser(
        description="Generuje githook na podstawie plików .gitlab-ci.yml"
    )
    parser.add_argument("--linters-img", help="Wersja obrazu z linterami")
    parser.add_argument("--qodana-img", help="Wersja obrazu z qodaną")
    parser.add_argument(
        "--main-branch", help="Główna gałąź repozytorium", required=True
    )
    return parser.parse_args()


def generate(main_branch: str, linters_version: str | None, qodana_version: str | None):
    HOOK_FILE = ".githooks/pre-commit.d/formatters.sh"
    linter_img_tasks = set()
    qodana_img_tasks = set()
    with open(".ci-env", "r", encoding="UTF-8") as env_file:
        for line in env_file.readlines():
            line = line.strip()
            if not line.startswith("export CI_ENABLED_FORMAT"):
                continue
            var, _, enabled = line.removeprefix("export ").partition("=")
            if enabled != "1":
                continue
            _, _, _, _, lang, tool = var.lower().split("_")
            task = f"format_{lang}_{tool}"
            if tool == "intellij":
                qodana_img_tasks.add(task)
            else:
                linter_img_tasks.add(task)

    with open(HOOK_FILE, "w", encoding="utf-8") as fp:
        fp.write("#!/usr/bin/env bash\n")
        fp.write("set -euo pipefail\n")
        fp.write("shopt -s nullglob\n")
        write_commands(
            fp,
            main_branch,
            linter_img_tasks,
            "bananawhite98/linters",
            linters_version,
        )
        write_commands(
            fp,
            main_branch,
            qodana_img_tasks,
            "bananawhite98/qodana",
            qodana_version,
            '-e _JAVA_OPTIONS="-Duser.home=/tmp"',
        )


def write_commands(
        fp: TextIO,
        main_branch: str,
        tasks: set[str],
        img_name: str,
        img_version: str | None,
        extra_docker_flags: str = "",
):
    if not tasks:
        return
    if img_version:
        user_id = (
            subprocess.run(["id", "-u"], check=True, capture_output=True)
            .stdout.decode("utf8")
            .strip()
        )
        group_id = (
            subprocess.run(["id", "-g"], check=True, capture_output=True)
            .stdout.decode("utf8")
            .strip()
        )
        fp.write(
            f'docker run {extra_docker_flags} -e GIT_EXEC_PATH=/usr/lib/git-core -u {user_id}:{group_id} -v"${{PWD}}":/src --rm {img_name}:{img_version} \\\n'
        )
        fp.write(
            f"     /usr/bin/bash -c 'cd /src && make -f /opt/linters/tools/Makefile MAIN_BRANCH={main_branch}"
        )
        fp.write(" ".join(tasks))
        fp.write("'\n\n")
    else:
        folder = Path(__file__).parent
        fp.write(f"make -f {folder}/Makefile MAIN_BRANCH={main_branch} ")
        fp.write(" ".join(tasks))
        fp.write("\n\n")


def main():
    args = cli()
    generate(args.main_branch, args.linters_img, args.qodana_img)


if __name__ == "__main__":
    main()
