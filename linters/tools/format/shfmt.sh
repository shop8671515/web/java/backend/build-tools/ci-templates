#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'
set -x
echo "shfmt $(shfmt --version)"
/usr/bin/shfmt --write "$@"