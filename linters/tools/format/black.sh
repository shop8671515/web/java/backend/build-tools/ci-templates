#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

black --version
black "$@"