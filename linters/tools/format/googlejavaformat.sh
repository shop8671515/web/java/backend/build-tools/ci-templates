#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

JAR="${HOME}/.cache/jars/googlejavaformat.jar"
if [[ -z ${CI+x} ]] && [[ ! -f "${JAR}" ]]; then
        VERSION=1.19.1
        mkdir -p "${HOME}/.cache/jars"
        wget "https://github.com/google/google-java-format/releases/download/v${VERSION}/google-java-format-${VERSION}-all-deps.jar" -O "${JAR}"
fi
java -jar ~/.cache/jars/googlejavaformat.jar --version
java -jar ~/.cache/jars/googlejavaformat.jar --skip-reflowing-long-strings --replace "$@"

