#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

command='idea.sh'
if command -v idea >/dev/null; then
	      command='idea'
fi

${command} format "$@"
