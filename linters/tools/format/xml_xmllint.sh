#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

xmllint --version

# shellcheck disable=SC2048
for file in $*; do
	xmllint --format --output "${file}" "${file}"
done
