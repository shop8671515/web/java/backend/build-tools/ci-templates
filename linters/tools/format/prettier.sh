#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "prettier $(prettier --version)"
prettier -w "$@"