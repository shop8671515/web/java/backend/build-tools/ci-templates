#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

echo "sql-formatter $(sql-formatter --version)"

options=' '
if [[ -f .sql-formatter.json ]]; then
  options=$'--config\t.sql-formatter.json'
else
  options=$'--config\t/tmp/sql-formatter.json'
  if [[ ! -f '/tmp/sql-formatter.json' ]]; then
      cat <<EOF >/tmp.sql/formatter.json

{
  "language": "postgresql",
  "keywordCase": "upper",
  "expressionWidth": 120,
  "tabWidth": 4
}
EOF
	fi
fi

# shellcheck disable=SC2048
for file in $*; do
  sql-formatter ${options} --output "${file}" "${file}"
done
