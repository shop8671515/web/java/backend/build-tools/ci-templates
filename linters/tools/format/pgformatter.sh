#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

pg_format --version
if [[ -f .pg_format ]]; then
  pg_format --config .pg_format --inplace "$@"
else
  pg_format --inplace "$@"
fi