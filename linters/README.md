# Obraz z linterami i formaterami.

Obraz zawiera [Makefile](./tools/Makefile), który jest używany w szablonie z
linterami, ale które jest też przeznaczone do wykorzystywania zewnętrznie, np.
w githookach. Pozwala to na uzyskanie takiego samego rezultatu, jak w CI i
reużywanie konfiguracji.

## Lintery

| linter                                               | raporty (w katalogu `build/reports`)     |
|------------------------------------------------------|------------------------------------------|
| [codenarc](https://codenarc.org)                     | codenarc.html, codenarc-codeclimate.json |
| [elint-sql](./tools/elint-sql)                       | (brak)                                   |
| [hadolint](https://github.com/hadolint/hadolint)     | hadolint.json (codeclimate)              |
| [mypy](https://github.com/python/mypy)               | mypy-codeclimate.json                    |
| [pylint](https://github.com/pylint-dev/pylint)       | pylint-codeclimate.json                  |
| [ruff](https://github.com/charliemarsh/ruff)         | ruff-codeclimate.json                    |
| [shellcheck](https://github.com/koalaman/shellcheck) | (brak)                                   |
| [yamllint](https://github.com/adrienverge/yamllint)  | (brak)                                   |

## Formatery

- [black](https://github.com/psf/black)
- [google java format](https://github.com/google/google-java-format)
- [intellij](https://www.jetbrains.com/help/idea/command-line-formatter.html)
- [pgformatter](https://github.com/darold/pgFormatter)
- [prettier](https://github.com/prettier/prettier)
- [shfmt](https://github.com/mvdan/sh)
- [sql-formatter](https://github.com/sql-formatter-org/sql-formatter)
- [xmllint](https://gnome.pages.gitlab.gnome.org/libxml2/xmllint.html)

### Konfiguracja

| narzędzie          | plik konfiguracyjny                                                                                                                     |
|--------------------|-----------------------------------------------------------------------------------------------------------------------------------------|
| black              | [pyproject.toml](https://black.readthedocs.io/en/stable/usage_and_configuration/the_basics.html#configuration-via-a-file)               |
| codenarc           | [codenarc.groovy](https://codenarc.org/StarterRuleSet-AllRules.groovy.txt), [opis](https://codenarc.org/codenarc-rule-index.html)       |
| google java format | brak                                                                                                                                    |
| intellij           | [.idea/codeStyles/{codeStyleConfig,Project}.xml](https://www.jetbrains.com/help/idea/configuring-code-style.html#import-export-schemes) |
| mypy               | [wiele możliwości](https://mypy.readthedocs.io/en/stable/config_file.html)                                                              |
| pgformatter        | [.pg_format](https://github.com/darold/pgFormatter/blob/master/doc/pg_format.conf.sample)                                               |
| prettier           | [wiele możliwości](https://prettier.io/docs/en/configuration.html)                                                                      |
| pylint             | [wiele możliwości](https://pylint.pycqa.org/en/latest/user_guide/usage/run.html#command-line-options)                                   |
| ruff               | [wiele możliwości](https://github.com/charliermarsh/ruff#configuration)                                                                 |
| shfmt              | [.editorconfig](https://github.com/mvdan/sh/blob/master/cmd/shfmt/shfmt.1.scd#examples)                                                 |
| sql-formatter      | [.sql-formatter.json](https://github.com/sql-formatter-org/sql-formatter#configuration-options)                                         |
| xmllint            | brak                                                                                                                                    |


### Łatka z poprawnym formatowaniem

W przypadku wykrycia problemów z formatowaniem, do zadania załączana
jest łatka, którą wykorzysta aplikacja w celu naprawy. Łatka generowania jest jedynie,
gdy liczba plików, w których wykryty problemy jest mniejsza niźli
`CI_FORMAT_MAX_FILES_DIFF` (zmienna ta jest opisana poniżej w sekcji
[Zmienne środowiskowe](#zmienne-środowiskowe)). Po jej pobraniu łatkę można
zastosować poleceniem:
```shell
git apply format.patch
```

Jest to użyteczne do jednorazowej poprawki, ale najwygodniej jest skonfigurować
githooki, żeby formatowanie było poprawiane automatycznie przy zapisie
rewizji, co jest opisane w sekcji [Generowanie githooków](#generowanie-githooków).

## Użycie szablonu

Szablon pozwala niezależnie włączać dowolną liczbę narzędzi. Domyślnie wszystkie
są wyłączone i należy je włączyć zmienną środowiskową. Konfiguracja `.gitlab-ci.yml`,
która włącza sprawdzanie formatowania plików markdown i komunikatów rewizji
w gicie może wyglądać tak:

```yml
include:
  - project: enigma/build-tools/ci-templates
    file: linters/v2.yml

variables:
  # konfiguracja szablonu linters/v2.yml
  CI_ENABLED_FORMAT_MD_PRETTIER: 1
  CI_ENABLED_LINT_GIT: 1
```

### Zmienne środowiskowe

| zmienna środowiskowa             | opis                                                                       |
|----------------------------------|----------------------------------------------------------------------------|
| `CI_ALWAYS_RUN`                  | Czy uruchamiać zawsze, nawet gdy nie wykryto zmienionych plików?           |
| `CI_FORMAT_MAX_FILES_DIFF`       | Max. liczba plików z błędami dla których generowana jest poprawkowa łatka. |
| `CI_LINTERS_VERSION`             | Wersja obrazu z linterami (raczej nie powinna być ręcznie zmieniana).      |
| `CI_LINT_ONLY_CHANGED`           | Czy sprawdzać wszystkie pliki, czy tylko te zmienione?                     |
| `CI_FILES_<TYP>`                 | Glob/lista plików, których zmiana uruchomi zadanie.                        |
| `CI_FILES_<TYP>_EXCLUDE`         | Wyrażenie regularne perla do pomijania plików z formatowania.              |
| `CI_ENABLED_FORMAT_<TYP>_<PROG>` | Czy sprawdzać formatowanie?                                                |
| `CI_ENABLED_LINT_<TYP>_<PROG>`   | Czy lintować pliki?                                                        |
| `CI_MAX_GIT_FILESIZE`            | Maksymalny dozwolony rozmiar dla nowego pliku w bajtach.                   |

`<TYP>` oznacza typ pliku, `<PROG>` narzędzie. Pełną listę wspieranych typów plików i narzędzi można znaleźć w [v2](./v2.yml).

## Generowanie githooków

Formatery są konfigurowalne i każdy może być niezależnie użyty. Z tego powodu
githook jest generowany i włączone są tylko te narzędzia, które są włączone
w .gitlab-ci.yml. Przed generacją należy sklonować albo zaktualizować to
repozytorium. Następnie w katalogu z projektem należy wywołać jedno z poniższych
poleceń.

Do generowania używany jest python (domyślnie używane jest polecenie `python3`).
Jeżeli komenda nazywa się inaczej albo nie ma jej w ścieżce można podać pełna
ścieżkę do interpretera `PY=/ścieżka/do/python3`.

1. Wersja używająca obraz dockerowy

```sh
make -f <ścieżka_do_tego_repozytorium>/linters/tools/Makefile \
    generate-githooks \
    MAIN_BRANCH="$(git symbolic-ref refs/remotes/origin/HEAD | sed 's|refs/remotes/origin/||')" \
    LINTERS_VERSION=1 QODANA_VERSION=1
```

, gdzie `LINTERS_VERSION` to pożądana wersja obrazu
`registry.enigma.com.pl/build-tools/linters`, a `QODANA_VERSION` – `registry.enigma.com.pl/build-tools/qodana`. Zmienne są niezależne, gdyż obraz z qodaną dużo waży, a praktycznie każdy ma zainstalowane Intellij IDEA, które może być użyte w miejsce qodany, ale ma to jeden minus. Jeżeli IDEA jest już uruchomiona, formatowanie zakończy się błędem. Problem nie występuje, gdy używana jest qodana.

2. Wersja wymagająca ręcznego zainstalowania narzędzi

```sh
make -f <ścieżka_do_tego_repozytorium>/linters/tools/Makefile \
    generate-githooks \
    MAIN_BRANCH="$(git symbolic-ref refs/remotes/origin/HEAD | sed 's|refs/remotes/origin/||')"
```

### Konfiguracja gita

```sh
git config core.hooksPath .githooks/
```

## Lokalne uruchamianie zadań

Szablon korzysta z pliku [Makefile](./tools/Makefile), który może być użyty do
wykonania zadań bez wysyłania kodu do GitLaba. Z tego pliku korzystają githooki,
ale może też być przydatny do wykonywania lintowania, dla przykładu:

```sh
make -f <ścieżka_do_tego_repozytorium>/linters/tools/Makefile \
    lint_groovy_codenarc
```

## Integracja z GitLabem

- [raport z lintowania kodu](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
- raporty HTML do pobrania (zadanie `lint_groovy_codenarc`)
- łatka z poprawnym formatowaniem do pobrania (każde zadanie `format_*`)

## Integracja z edytorami

### Intellij

#### CodeNarc

Należy zainstalować [wtyczkę](https://plugins.jetbrains.com/plugin/5925-codenarc).
Reguły konfigurowane są tak samo, jak wbudowane reguły Intellij, czyli w
`File -> Settings -> Editor -> Inspections` w sekcji `CodeNarc`.
