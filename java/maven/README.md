# Szablon do budowania projektów maven

## Użycie

Przykładowy plik `.gitlab-ci.yml` może wyglądać tak:

```yaml
include:
  - project: "https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates"
    file: "java/maven/v1.yml"

variables:
  # konfiguracja szablonu maven
  JAVA_CI_VERSION: 11
  JAVA_CI_LINT_QODANA: 1
  JAVA_CI_LINT_SONAR: 1
  JAVA_CI_FORMAT: 1
```

## Ogólna konfiguracja

Szablon jest napisany pod projekty, które korzystają z generycznego poma, który
jest utrzymywany i udokumentowany w [osobnym repozytorium][java-shared]. W tymże
repozytorium przechowywany są również pomocnicze pliki, w tym Makefile do
lokalnego uruchamiania Qodany, a także opis, jak zintegrować niektóre z narzędzi
(głównie lintery) z edytorem/IDE.

Zadania można konfigurować za pomocą zmiennych środowiskowych, jak jest
zademonstrowane powyżej.

| zmienne środowiskowa     | opis                                                                                             |
|--------------------------|--------------------------------------------------------------------------------------------------|
| `JAVA_CI_VERSION`        | Wersja JDK do użycia                                                                             |
| `JAVA_CI_PROFILES`       | Lista profili mvn, które są aktywne podczas uruchamiania                                         |
| `JAVA_CI_ALWAYS_RUN`     | Wymuś uruchomienie kroku                                                                         |
| `JAVA_CI_LATEST_LTS_JDK` | Wersja najnowszego JDK (używane przez narzędzia, które wymagają nowych wersji JDK do działania). |
| `MAVEN_CLI_OPTS`         | Dodatkowe opcje przekazywane przy wywołanie `mvn`                                                |

W przypadku wartości, które oczekują wartości logicznej, `1` oznacza prawdę,
a `0` fałsz. Na końcu dziennika zadania jest wypisywany odnośnik do dokumentacji
zadania w tym pliku.

## Zadania

### test

|                       |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| Opis                  | uruchamia testy                                                      |
| Tryb wykonywania      | recenzja                                                             |
| Lokalne uruchamienie  | `mvn -P ci test`                                                     |
| Faza CI               | `test`                                                               |
| Integracja z GitLabem | [pokrycie kodu][pokrycie], [raport z wykonania testów][testy-raport] |


#### Konfiguracja

| zmienna środowiskowa | opis                |
| -------------------- |---------------------|
| `JAVA_CI_TEST`       | Czu uruchomić testy |

### style

|                        |                                                                                                                                      |
|------------------------|--------------------------------------------------------------------------------------------------------------------------------------|
| Opis                   | sprawdza formatowanie kodu za pomocą Google Java Format (GJF)                                                                        |
| Tryb wykonywania       | recenzja                                                                                                                             |
| Lokalne uruchomienie   | `java -jar <JAR GJF> --replace --skip-reflowing-long-strings $(find -name '*.java' -not -path '**/target/*' -not -path './.mvn/*')`  |
| Faza CI                | `test`                                                                                                                               |
| Integracja z GitLabem  | łatka w artefaktach, gdy kod nie jest poprawnie sformatowany                                                                         |


#### Konfiguracja
| zmienna środowiskowa            | opis                                                                                           |
| ------------------------------- |------------------------------------------------------------------------------------------------|
| `JAVA_CI_FORMAT`                | Czy sprawdzać formatowanie kodu?                                                               |
| `JAVA_CI_FORMAT_MAX_FILES_DIFF` | Maksymalna liczba plików z niepoprawnym formatowaniem dla których wygenerowana zostanie łatka. |

### lint

|                       |                                    |
| --------------------- | ---------------------------------- |
| Opis                  | uruchamia podstawowe lintery       |
| Tryb wykonywania      | recenzja                           |
| Lokalne uruchomienie  | `mvn -P ci -DskipTests verify`     |
| Faza CI               | `test`                             |
| Integracja z GitLabem | [raport z lintowania][lint-raport] |

Jeżeli używany jest [SonarQube][], do polecenia należy dodać
`-Dsonar.login=<żeton>`, który można stworzyć [na stronie konta](https://sonarcloud.io/account/security).

Lintowanie w CI nie jest przerywane, gdy w module zostaną wykryte błędy.
Lintowany jest zawszy cały projekt i dopiero na końcu zgłaszane są wszystkie
błędy. Jeżeli wykryte zostały jakiekolwiek problemy, zostaną one wypisane na
końcu dziennika zadania w skróconej formie.

#### Konfiguracja

| zmienna środowiskowa | opis                                   |
| -------------------- | -------------------------------------- |
| `JAVA_CI_LINT`       | Czy uruchamiać lintowanie?             |
| `JAVA_CI_LINT_SONAR` | Czy uruchamiać lintowanie w SonarQube? |

### lint_qodana

|                       |                                                                |
| --------------------- | -------------------------------------------------------------- |
| Opis                  | uruchamia lintowanie Qodaną/Intellij                           |
| Tryb wykonywania      | recenzja                                                       |
| Lokalne uruchomienie  | `make -f <ścieżka/do/java/shared/Maven.Makefile> qodana`       |
| Faza CI               | `test`                                                         |
| Integracja z GitLabem | [raport z lintowania][lint-raport], raport SARIF w artefaktach |

Raport jest publikowany i można go pobrać ze strony zadania `lint_qodana` albo
z głównej strony recenzji.

Pobrany raport można wczytać w Intellij po zainstalowania wtyczki `Qodana` w menu
`Tools -> Qodana -> Open Qodana Analysis Report...`

Istnieje też możliwość uruchomienia linterów bezpośrednio w Intellij w menu `Code -> Inspect Code`.

#### Konfiguracja

| zmienna środowiskowa          | opis                                            |
| ----------------------------- | ----------------------------------------------- |
| `JAVA_CI_LINT_QODANA`         | Czy uruchamiać lintowanie Qodaną?               |
| `JAVA_CI_LINT_QODANA_PROFILE` | Profil z konfiguracją linterów do uruchomienia. |

Szablon zawiera domyślny plik z [konfiguracją
Qodany](https://www.jetbrains.com/help/qodana/qodana-yaml.html#Set+up+a+profile). Jeżeli jest on niewystarczający, należy dodać plik `qodana.yaml` w głównym
katalogu repozytorium z własnymi ustawieniami. Wbudowana konfiguracja pozwala
na zmianę profilu z ustawieniami przez zmienną `JAVA_CI_LINT_QODANA_PROFILE`.
[Oficjalna dokumentacja konfiguracji](https://www.jetbrains.com/help/idea/customizing-profiles.html) opisuje, jak można dodać do projektu własne
ustawienia reguł z IDE.

Powszechnie stosowaną nazwą dla profilu jest `rszewczyk`. Pliki profilu projektu
należy następnie dodać do repozytorium:

```sh
git add -f .idea/inspectionProfiles/Project_Default.xml
git add -f .idea/inspectionProfiles/rszewczyk.xml
git add -f .idea/inspectionProfiles/profiles_settings.xml
git add -f .idea/misc.xml
```

### owasp-dependency-check

Sprawdza, czy zależności projektu zawierają znane podatności. Do sprawdzenia
używana jest wtyczka [dependency-check](https://github.com/jeremylong/DependencyCheck)

|                       |                                                |
| --------------------- | ---------------------------------------------- |
| Opis                  | sprawdza, czy zależności są podatne            |
| Tryb wykonywania      | recenzja                                       |
| Lokalne uruchomienie  | `mvn -P owasp -DskipTests`                     |
| Faza CI               | `test`                                         |
| Integracja z GitLabem | na razie brak, możliwa jest publikacja raportu |

#### Konfiguracja

| zmienna środowiskowa             | opis                                   |
| -------------------------------- | -------------------------------------- |
| `JAVA_CI_OWASP_DEPENDENCY_CHECK` | Czy uruchamiać sprawdzenie zależności? |

### badges

|                       |                                                            |
| --------------------- | ---------------------------------------------------------- |
| Opis                  | publikuje odznaki                                          |
| Tryb wykonywania      | domyślna gałąź                                             |
| Lokalne uruchomienie  | brak                                                       |
| Faza CI               | `deploy`                                                   |
| Integracja z GitLabem | odznaki można dodać do nagłówka projektu albo pliku README |

Publikowane są następujące odznaki:

| Opis                 | Nazwa pliku              |
| -------------------- | ------------------------ |
| Całkowite pokrycie   | coverage-total.svg       |
| Pokrycie rozgałęzień | coverage-branch.svg      |
| Pokrycie klas        | coverage-class.svg       |
| Pokrycie złożoności  | coverage-complexity.svg  |
| Pokrycie instrukcji  | coverage-instruction.svg |
| Pokrycie linii       | coverage-line.svg        |
| Pokrycie metod       | coverage-method.svg      |
| Liczba linii kodu    | lines.svg                |
| Wersja projektu      | version.svg              |

Całkowite pokrycie jest równoznaczne z pokryciem instrukcji, ale ma krótszy
opis.

W dzienniku jest wypisywany adres odznaki z całkowitym pokryciem kodu. W celu
użycia innej odznaki należy podmienić nazwę pliku.

#### Konfiguracja

| zmienna środowiskowa | opis                           |
| -------------------- | ------------------------------ |
| `JAVA_CI_PAGES`      | Czy publikować strony/odznaki? |

### artifactory

|                       |                     |
| --------------------- | ------------------- |
| Opis                  | publikuje artefakty |
| Tryb wykonywania      | domyślna gałąź      |
| Lokalne uruchomienie  | `mvn deploy`        |
| Faza CI               | `deploy`            |
| Integracja z GitLabem | brak                |

#### Konfiguracja

| zmienna środowiskowa      | opis                                                      |
| ------------------------- |-----------------------------------------------------------|
| `JAVA_CI_DEPLOY`          | Czy publikować artefakty?                                 |
| `JAVA_CI_DEPLOY_PROFILES` | Lista profili mvn, które są aktywne podczas publikowania. |


[java-shared]: https://gitlab.com/shop8671515/web/java/backend/java_shared
[lint-raport]: (https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html)
[pokrycie]: https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html
[SonarCloud]: https://sonarcloud.io/
[testy-raport]: https://docs.gitlab.com/ee/ci/unit_test_reports.html
