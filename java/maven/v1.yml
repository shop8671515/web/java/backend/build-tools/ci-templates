services:
  - docker:24.0.5-dind

variables:
  JAVA_CI_VERSION: 8
  JAVA_CI_DOCKER_IMAGE_VERSION: 2
  JAVA_CI_LINT: 1
  JAVA_CI_LINT_ALWAYS: 0
  JAVA_CI_LINT_SONAR: 0
  JAVA_CI_LINT_QODANA: 0
  JAVA_CI_LINT_QODANA_PROFILE: qodana.recommended
  JAVA_CI_OWASP_DEPENDENCY_CHECK: 0
  JAVA_CI_TEST: 1
  JAVA_CI_FORMAT: 1
  JAVA_CI_FORMAT_MAX_FILES_DIFF: 15
  JAVA_CI_DEPLOY: 1
  JAVA_CI_PAGES: 1
  JAVA_CI_PROFILES: ci
  JAVA_CI_DEPLOY_PROFILES: "!jacoco"
  JAVA_CI_LSIF: 0
  JAVA_CI_ALWAYS_RUN: 0
  JAVA_CI_LATEST_LTS_JDK: 21
  JAVA_CI_QODANA_JAVA_HOME: /usr/lib/jvm/java-1.17.0-openjdk-amd64/
  MAVEN_CLI_OPTS: --no-transfer-progress
  DOCKER_HOST: "tcp://docker:2375"
  DOCKER_DRIVER: overlay2

test:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  cache:
    - key: mvn
      paths:
        - .mvn/repository
  stage: test
  interruptible: true
  coverage: "/Pokrycie ogólne: ([0-9]{1,3})%/"
  tags:
    - gitlab-org-docker
  rules:
    - if: '$JAVA_CI_TEST == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_TEST == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  dependencies: []
  artifacts:
    paths:
      - coverage
      - public
    reports:
      junit: "**/target/surefire-reports/TEST-*.xml"
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura.xml
  script:
    - mvn $MAVEN_CLI_OPTS -P ${JAVA_CI_PROFILES} -s .mvn/settings.xml clean test
    - coverage_module=ci
    - JACOCO_REPORT="${coverage_module}/target/site/jacoco-aggregate/jacoco.xml"
    - |
      if [[ ! -f "${JACOCO_REPORT}" ]]; then
        JACOCO_REPORT="${coverage_module}/target/site/jacoco/jacoco.xml";
        if [[ ! -d "${JACOCO_REPORT}" ]]; then
          JACOCO_REPORT=jacoco-merged.xml
          jacoco-report merge $(find . -type f -name jacoco.xml) -o "${JACOCO_REPORT}"
        fi
      fi
    - jacoco-report summary ${JACOCO_REPORT} --json coverage.json
    - mkdir -p coverage/
    - jacoco2cobertura "${JACOCO_REPORT}" >| coverage/cobertura.xml
    - badges --no-version --json coverage.json --out "public/badges/${CI_COMMIT_SHA}"
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"

style:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  stage: test
  interruptible: true
  dependencies: []
  artifacts:
    paths:
      - format.patch
    when: on_failure
    expose_as: "Format patch"
  rules:
    - if: '$JAVA_CI_FORMAT == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_FORMAT == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - sudo google-java-format --version
    - |
      sudo if ! google-java-format --replace --set-exit-if-changed --skip-reflowing-long-strings $(find -name '*.java' -type f -not -path '**/target/*' -not -path './.mvn/*'); then
        count=$(git ls-files --modified | wc -l);
        if [[ ${count} -le ${JAVA_CI_FORMAT_MAX_FILES_DIFF} ]]; then
          git diff > format.patch;
          PAGER= git diff --color=always;
        else
          git ls-files --modified;
        fi;
        exit 1;
      fi
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"

lint:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  cache:
    - key: mvn
      paths:
        - .mvn/repository
    - key: sonar
      paths:
        - .sonar
  stage: test
  interruptible: true
  dependencies: [ ]
  artifacts:
    name: reports
    expire_in: 3 hrs
    paths:
      - ustawienia.html
      - "**/target/spotbugsXml.xml"
    reports:
      codequality: codeclimate.json
  rules:
    - if: '$JAVA_CI_LINT == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_LINT == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && ($JAVA_CI_ALWAYS_RUN == "1" || $JAVA_CI_LINT_ALWAYS == "1")'
  script:
    - mvn $MAVEN_CLI_OPTS -P ${JAVA_CI_PROFILES} -s .mvn/settings.xml
      -DskipTests
      -Dverify.fail=false
      -Djacoco.skip
      verify
    - TOKEN=''
    - if [[ ${JAVA_CI_VERSION} -gt 8 && ${JAVA_CI_LINT_SONAR} != 0 ]]; then
      TOKEN=${SONAR_TOKEN};
      mvn $MAVEN_CLI_OPTS -P ${JAVA_CI_PROFILES} -s .mvn/settings.xml
      -DskipTests
      -Dverify.fail=false
      -Djacoco.skip
      -Dspotbugs.skip
      -Dpmd.skip
      -Dsonar.login=${TOKEN}
      -Dsonar.token=${TOKEN}
      -Dsonar.host=${SONAR_HOST}
      verify || true;
      fi
    - sudo java2codeclimate --spotbugs $(find . -type f -name 'spotbugsXml.xml')
      --pmd $(find . -type f -name 'pmd.xml' | grep -v rulesets)
      --ktlint $(find . -type f -name 'ktlint-report.json')
      --sonar-token "${TOKEN}"
      --quickfix-file -
      -o codeclimate.json
    - propdoc ustawienia.html
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"
    - groupId="$(pompy --tag groupId)"
    - rm -rf ".mvn/repository/${groupId//./\/}"

lint_qodana:
  image: bananawhite98/qodana:${CI_LINTERS_VERSION}
  cache:
    - key: mvn
      paths:
        - .mvn/repository
    - key: mvn_wrapper
      paths:
        - .mvn/wrapper/dists/
  stage: test
  interruptible: true
  dependencies: []
  artifacts:
    reports:
      codequality: qodana-codeclimate.json
    paths:
      - qodana/qodana.sarif.json
    when: on_failure
    expose_as: "Qodana SARIF report"
  rules:
    - if: '$JAVA_CI_LINT_QODANA == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_LINT_QODANA == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - sudo mkdir -p ~/.m2
    - sudo cp -f .mvn/settings.xml ~/.m2/settings.xml
    - |
      if [[ ! -f qodana.yaml ]]; then
      cat << EOF > qodana.yaml
      profile:
        name: ${JAVA_CI_LINT_QODANA_PROFILE}
      failThreshold: 0
      exclude:
        - name: All
          paths:
          - .mvn
          - qodana
          - build
      EOF
      fi
    - |
      MVN=mvn
      if [[ -x mvnw ]]; then
        MVN="./mvnw"
      fi
    - JAVA_HOME=${JAVA_CI_QODANA_JAVA_HOME} java --version
    - JAVA_HOME=${JAVA_CI_QODANA_JAVA_HOME} ${MVN} -s .mvn/settings.xml $MAVEN_CLI_OPTS clean compile
    - /opt/idea/bin/qodana
      --property=idea.headless.enable.statistics=false
      --results-dir="${CI_PROJECT_DIR}/qodana"
      --cache-dir="${CI_PROJECT_DIR}/qodana/cache" || exit_code=$?
    - |
      if [[ ${exit_code} -ne 0 ]]; then
        printf "Qodana wykryła problemy. Informacje, jak pobrać i wczytać raport można znaleźć pod adresem\n";
        printf "https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#qodana\n"
        printf "oraz https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"
        report2codeclimate --sarif $(find . -name qodana.sarif.json) --quickfix-file - -o qodana-codeclimate.json
        exit 1
      fi

owasp-dependency-check:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  cache:
    - key: mvn
      policy: pull
      paths:
        - .mvn/repository
    - key: owasp
      paths:
        - .mvn/repository/org/owasp/dependency-check-data
  stage: test
  interruptible: true
  dependencies: []
  rules:
    - if: '$JAVA_CI_OWASP_DEPENDENCY_CHECK == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_OWASP_DEPENDENCY_CHECK == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - mvn $MAVEN_CLI_OPTS -P owasp -s .mvn/settings.xml
      -DskipTests
      verify
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"

java_lsif:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  cache:
    - key: mvn
      paths:
        - .mvn/repository
  stage: build
  interruptible: true
  allow_failure: true
  dependencies: []
  artifacts:
    reports:
      lsif: dump.lsif
  rules:
    - if: '$JAVA_CI_LSIF == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
    - if: '$JAVA_CI_LSIF == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - scip-java index -- clean verify -DskipTests -s .mvn/settings.xml
    - scip convert

artifactory:
  image: bananawhite98/java${JAVA_CI_VERSION}:${JAVA_CI_DOCKER_IMAGE_VERSION}
  cache:
    - key: mvn
      paths:
        - .mvn/repository
  dependencies: []
  stage: deploy
  rules:
    - if: '$JAVA_CI_DEPLOY == "1" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$JAVA_CI_DEPLOY == "1" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $JAVA_CI_ALWAYS_RUN == "1" && $CI_PIPELINE_SOURCE != "merge_request_event"'
  script:
    - mvn $MAVEN_CLI_OPTS -P ${JAVA_CI_DEPLOY_PROFILES} -s .mvn/settings.xml
      -DskipTests=true
      -Dspotbugs.skip=true
      -Dpmd.skip=true
      -Djib.disableUpdateChecks=true
      deploy
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/maven#${CI_JOB_NAME}\n"

