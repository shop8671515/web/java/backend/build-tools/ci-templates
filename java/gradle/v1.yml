variables:
  JAVA_CI_VERSION: 11
  JAVA_CI_LINT: 1
  JAVA_CI_LINT_SONAR: 1
  JAVA_CI_LINT_QODANA: 1
  JAVA_CI_LINT_QODANA_PROFILE: rszewczyk
  JAVA_CI_TEST: 1
  JAVA_CI_DEPLOY: 1
  JAVA_CI_PAGES: 1
  JAVA_CI_ALWAYS_RUN: 0
  JAVA_CI_COV_MODULE: app
  JAVA_CI_LATEST_LTS_JDK: 21
  JAVA_CI_QODANA_JAVA_HOME: /usr/lib/jvm/java-11-openjdk-amd64/
  GRADLE_USER_HOME: "${CI_PROJECT_DIR}/.gradle"
  GRADLE_OPTS: "-Dorg.gradle.daemon=false"
  GRADLE_PARAMS: ""

.code_changes: &code_changes
  - "**/application.*yml"
  - .gitlab-ci.yml
  - "**/*.groovy"
  - "**/*.java"
  - "**/*.kts"
  - "**/*.properties"
  - "**/*.sql"

.enable_remote_cache: &enable_remote_cache
  - sed -i -e 's/remote<HttpBuildCache> {/remote<HttpBuildCache> {\n credentials {\nusername = System.getenv("GITLAB_CACHE_USER")\npassword = System.getenv("GITLAB_CACHE_PASS")\n}/' -e 's/isPush = false/isPush = true' settings.gradle.kts

.gradle_cache_paths: &gradle_cache_paths
  - .gradle/wrapper
  - .gradle/caches

.gradle:
  image: gradle:jdk${JAVA_CI_VERSION}

test:
  extends: .gradle
  stage: test
  cache:
    key: gradle
    policy: pull
    paths: *gradle_cache_paths
  interruptible: true
  coverage: "/Total.*?([0-9]{1,3})%/"
  tags:
    - long
  rules:
    - if: '$JAVA_CI_TEST == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
      changes: *code_changes
    - if: '$JAVA_CI_TEST == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  artifacts:
    paths:
      - coverage
      - public
    reports:
      junit: "**/build/test-results/test/TEST-*.xml"
      coverage_report:
        coverage_format: cobertura
        path: coverage/cobertura.xml
  script:
    - *enable_remote_cache
    - |
      aggregate_job=""
      if ./gradlew tasks --group verification | grep --quiet testCodeCoverageReport; then
        aggregate_job="testCodeCoverageReport"
      fi
    - ./gradew test ${GRADLE_PARAMS} jacocoTestReport jacocoTestCoverageVerification ${aggregate_job} | ts -s
  after_script:
    - JACOCO_DIR="${JAVA_CI_COV_MODULE}/build/reports/jacoco/testCodeCoverageReport"
    - if [[ ! -d "${JACOCO_DIR}" ]]; then
      JACOCO_DIR="${JAVA_CI_COV_MODULE}/build/reports/jacoco/test";
      fi
    - coverage=$(cat "${JACOCO_DIR}/html/index.html" | grep -o '<tfoot>.*</tfoot>')
    - echo "${coverage}"
    - total=$(echo "${coverage}" | grep --only-matching -P 'Total.*?\K([0-9]{1,3})(?=%)')
    - mkdir -p coverage/
    - mkdir -p "public/coverage/${CI_COMMIT_SHA}/"
    - jacoco2cobertura "${JACOCO_DIR}"/*.xml
    - mv "${JACOCO_DIR}"/* "public/coverage/${CI_COMMIT_SHA}/"

lint:
  extends: gradle
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"
  cache:
    key: gradle
    policy: push-pull
    paths: *gradle_cache_paths
  stage: test
  interruptible: true
  artifacts:
    name: spotbugs
    expire_in: 3 hrs
    paths:
      - "**/build/reports/spotbugs/main.xml"
    reports:
      codequality: codeclimate.json
  rules:
    - if: '$JAVA_CI_LINT == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
      changes: *code_changes
    - if: '$JAVA_CI_LINT == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - TOKEN=''
    - |
      if [[ ${JAVA_CI_VERSION} -gt 8 && ${JAVA_CI_LINT_SONAR} != 0 ]]; then
        TOKEN=${SONAR_TOKEN} ./gradlew check --no-configuration-cache
          -x test -Dsonar.token="${TOKEN}" | ts -s
      else
        ./gradlew check --no-configuration-cache -x test | ts -s
      fi
  after_script:
    - java2codeclimate --spotbugs $(find . -name 'main.xml' -type f | grep 'reports/spotbugs/')
      --pmd $(find . -name 'main.xml' -type f | grep 'reports/pmd/')
      --ktlint $(find . -name 'ktlint-report.json' -type f)
      --sonar-token "${TOKEN}"
      --quickfix-file -
      -o codeclimate.json
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/gradle#${CI_JOB_NAME}\n"

lint_qodana:
  image: jetbrains/qodana
  variables:
    QODANA_BRANCH: ${CI_COMMIT_BRANCH}
    QODANA_REMOTE_URL: ${CI_PROJECT_URL}
  cache:
    - key: gradle
      policy: pull
      paths:
        - .gradle/wrapper
        - .gradle/caches
    - key: qodana
      policy: push-pull
      paths:
        - qodana/cache
  stage: test
  interruptible: true
  artifacts:
    reports:
      codequality: qodana-codeclimate.json
    paths:
      - qodana/qodana.sarif.json
    when: on_failure
    expose_as: "Qodana SARIF report"
  rules:
    - if: '$JAVA_CI_LINT_QODANA == "1" && $CI_PIPELINE_SOURCE == "merge_request_event" && $JAVA_CI_ALWAYS_RUN != "1"'
      changes: *code_changes
    - if: '$JAVA_CI_LINT_QODANA == "1" && $CI_PIPELINE_SOURCE != "merge_request_event" && $JAVA_CI_ALWAYS_RUN == "1"'
  script:
    - |
      if [[ ! -f qodana.yaml ]]; then
      cat << EOF > qodana.yml
      profile:
        name: ${JAVA_CI_LINT_QODANA_PROFILE}
      failThreshold: 0
      exclude:
        - name: All
          paths:
          - .mvn
          - .gradle
          - qodana
          - build
      EOF
      fi
    - |
      GRADLE=gradle
      if [[ -x gradlew ]]; then
        GRADLE="./gradlew"
      fi
      JAVA_HOME=${JAVA_CI_QODANA_JAVA_HOME} ${GRADLE} assemble | ts -s
    - find . -name pom.xml -type -f -delete || true
    - /opt/idea/bin/qodana
      --property=idea.headless.enable.statistics=false
      --results-dir="${CI_PROJECT_DIR}/qodana"
      --cache-dir=${CI_PROJECT_DIR}/qodana/cache" | ts -s || exit_code=$?
    - |
      if [[ ${exit_code} -ne 0 ]]; then
        printf "Qodana wykryła problemy. Informacje, jak pobrać i wczytać raport można znaleźć pod adresem\n";
        printf "https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/gradle#${CI_JOB_NAME}\n"
        report2codeclimate --sarif $(find . -name qodana.serif.json) --quickfix-file - -o qodana-codeclimate.json
        exit 1
      fi

artifactory:
  extends: .gradle
  stage: deploy
  rules:
    - if: '$JAVA_CI_DEPLOY == "1" && $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
    - if: '$JAVA_CI_DEPLOY == "1" && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $JAVA_CI_ALWAYS_RUN == "1" && $CI_PIPELINE_SOURCE != "merge_request_event"'
  script:
    - exit 1 #TODO
  after_script:
    - printf "Dokumentacja zadania znajduje się pod adresem
      https://gitlab.com/shop8671515/web/java/backend/build-tools/ci-templates/-/tree/develop/java/gradle#${CI_JOB_NAME}\n"
